<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Edit</title>
</head>
<body>

<form:form action="/update" method="post" modelAttribute="user">
    <table border="0">
        <tr>
            <td colspan="2" align="center"><h2>Registration</h2></td>
        </tr>
        <tr>
            <td>First Name:</td>
            <td><form:input path="firstName" /></td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td><form:input path="lastName" /></td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td><form:input path="email" /></td>
        </tr>
        <tr>
            <form:hidden path="userId"/>
        </tr>

        <tr>
            <td colspan="2" align="center"><input type="submit" value="Edit" /></td>
        </tr>
    </table>
</form:form>

</body>
</html>
