<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List All User</title>
</head>
<body>

<c:if test = "${!empty userList}">
    <table>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Settings</th>
        </tr>

        <c:forEach items ="${userList}" var ="user">
            <tr>
                <th>${user.userId}</th>
                <th>${user.firstName}</th>
                <th>${user.lastName}</th>
                <th>${user.email}</th>
                <th><a href="<c:url value='detail/${user.userId}'/>">Detail</a></th>
                <th><a href="<c:url value='edit/${user.userId}'/>">Edit</a></th>
                <th><a href="<c:url value='delete/${user.userId}'/>">Delete</a></th>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>
