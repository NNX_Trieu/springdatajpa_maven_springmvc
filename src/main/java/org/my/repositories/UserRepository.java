package org.my.repositories;

import org.my.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by trieulieuf9 on 2/12/17.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
