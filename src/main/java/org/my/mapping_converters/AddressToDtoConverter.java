package org.my.mapping_converters;

import org.dozer.DozerConverter;
import org.my.models.User;
import org.my.services_imp.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;



public class AddressToDtoConverter extends DozerConverter<User, Long> {
    @Autowired
    UserServiceImp userServiceImp;

    public AddressToDtoConverter() {
        super(User.class, Long.class);
    }


    public Long convertTo(User user, Long userId) {
        return user.getUserId();
    }

    public User convertFrom(Long userId, User user) {
        return userServiceImp.find(userId);
    }
}
