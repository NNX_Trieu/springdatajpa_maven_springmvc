package org.my.services;

import org.my.models.Address;
import org.my.models.User;

import java.util.List;

/**
 * Created by trieulieuf9 on 2/13/17.
 */
public interface AddressService {
    public Address create(Address address);
    public Address update(Address address);
    public void delete(Address address);
    public void deleteById(long id);
    public Address find(long id);
    public List<Address> getAll();
}
