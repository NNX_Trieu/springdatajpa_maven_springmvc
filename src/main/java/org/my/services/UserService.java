package org.my.services;

import org.my.models.User;

import java.util.List;

/**
 * Created by trieulieuf9 on 2/13/17.
 */
public interface UserService {
    public User create(User user);
    public User update(User user);
    public void delete(User user);
    public void deleteById(long userId);
    public User find(long userId);
    public List<User> getAll();
}