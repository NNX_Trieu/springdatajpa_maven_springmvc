package org.my.controllers;

import org.my.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.my.services_imp.UserServiceImp;

import java.util.List;
import java.util.Map;


@Controller
@Transactional
public class RegisterController {

    @Autowired
    UserServiceImp userServiceImp;

    @RequestMapping(value = "/", method= RequestMethod.GET)
    public String AllUser(Map<String, Object> map) {

        List<User> userList = userServiceImp.getAll();
        map.put("userList", userList);

        return  "all_user_list";
    }

    @RequestMapping(value = "/register", method= RequestMethod.GET)
    public String Register(Map<String, Object> map) {
        map.put("user", new User());
        return "register";
    }

    @RequestMapping(value = "/create", method= RequestMethod.POST)
    public String Create(User user, Map<String, Object> map) {
        User usr = userServiceImp.create(user);
        return  "redirect:detail/" + usr.getUserId();
    }

    @RequestMapping(value = "/detail/{userId}", method= RequestMethod.GET)
    public String Detail(@PathVariable("userId") long userId, Map<String, Object> map) {

        User user = userServiceImp.find(userId);

        map.put("firstName", user.getFirstName());
        map.put("lastName", user.getLastName());
        map.put("email", user.getEmail());

        return  "detail";
    }

    @RequestMapping(value = "/edit/{userId}", method= RequestMethod.GET)
    public String Edit(@PathVariable("userId") long userId,Map<String, Object> map) {

        map.put("user", userServiceImp.find(userId));
        return "edit";
    }

    @RequestMapping(value = "/update", method= RequestMethod.POST)
    public String Update(User user, Map<String, Object> map) {

        User usr = userServiceImp.create(user);
        return  "redirect:detail/" + usr.getUserId();
    }

    @RequestMapping(value = "/delete/{userId}", method= RequestMethod.GET)
    public String Delete(@PathVariable("userId") long userId, Map<String, Object> map) {
        System.out.println("checkpoint 1");

        userServiceImp.deleteById(userId);
        return  "redirect:/";
    }

}
