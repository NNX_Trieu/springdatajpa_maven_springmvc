package org.my.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.my.dto.AddressDTO;
import org.my.models.Address;
import org.my.models.User;
import org.my.services.AddressService;
import org.my.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Transactional
@Controller
public class AddressApiController {

    @Autowired
    UserService userService;

    @Autowired
    AddressService addressService;

    // Create
    @RequestMapping(value = "/api/{userId}/address", method = RequestMethod.POST)
    public @ResponseBody String createAddress(@RequestBody Address address, @PathVariable("userId") long userId){

        User user = userService.find(userId);
        address.setUser(user);

        System.out.println(address.getAddress());
        System.out.println(address.getPhoneNumber());

        addressService.create(address);
        return "something";
    }


    // Show
    @Autowired
    DozerBeanMapper dozerMapperBean;

    @RequestMapping(value = "/api/{userId}/address", method = RequestMethod.GET)
    public @ResponseBody List<AddressDTO> showAddresses(@PathVariable("userId") long userId) throws JsonProcessingException {
        User user = userService.find(userId);
        Set<Address> addresses = user.getAddresses();

        List<AddressDTO> listDto = new ArrayList<AddressDTO>();

        for (Address address: addresses) {
            AddressDTO dto = dozerMapperBean.map(address, AddressDTO.class);

            listDto.add(dto);
        }

        return listDto;
    }


    // Edit
    @RequestMapping(value = "/api/{userId}/address", method = RequestMethod.PUT)
    public @ResponseBody String editAddress(@RequestBody Address address, @PathVariable("userId") long userId){

        User user = userService.find(userId);
        address.setUser(user);

        addressService.create(address);
        return "something";
    }


    // Delete
    @RequestMapping(value = "/api/{userId}/address/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String deleteAddress(@PathVariable("id") long id){

        addressService.deleteById(id);
        return "something";
    }


//    @RequestMapping(value = "/test", method= RequestMethod.GET)
//    public @ResponseBody String test() {
//        User user = userService.find((long) 1);
//
//        user.setEmail("trieulieuf9@gmail.com");
//
//        return "something";
//    }

}
