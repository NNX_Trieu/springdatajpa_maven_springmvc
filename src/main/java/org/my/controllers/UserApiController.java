package org.my.controllers;

import org.my.models.User;
import org.my.services_imp.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Transactional
@Controller
public class UserApiController {

    @Autowired
    UserServiceImp userServiceImp;

    @RequestMapping(value = "/api/user", method = RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        return userServiceImp.create(user);
    }

    @RequestMapping(value = "/api/user/{userId}", method = RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable("userId") long userId) {
        return userServiceImp.find(userId);
    }

    @RequestMapping(value = "/api/user/{userId}", method = RequestMethod.DELETE)
    public long deleteUserById(@PathVariable("userId") long userId) {
        userServiceImp.deleteById(userId);
        return userId;
    }

    @RequestMapping(value = "/api/user", method = RequestMethod.GET)
    public @ResponseBody List<User> getAllUser() {
        return userServiceImp.getAll();
    }

    @RequestMapping(value = "/api/user", method = RequestMethod.PUT)
    public @ResponseBody User editUser(@RequestBody User user) {
        return userServiceImp.update(user);
    }

}
