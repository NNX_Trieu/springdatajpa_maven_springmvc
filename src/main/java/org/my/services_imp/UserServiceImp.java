package org.my.services_imp;

import org.my.models.User;
import org.my.repositories.UserRepository;
import org.my.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserRepository userRepository;

    public User create(User user) {
        return userRepository.save(user);
    }

    public User update(User user) {
        return userRepository.save(user);
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public void deleteById(long userId) {
        userRepository.delete(userId);
    }

    public User find(long userId) {
        return userRepository.findOne(userId);
    }

    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }
}
