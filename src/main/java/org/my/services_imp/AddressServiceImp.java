package org.my.services_imp;

import org.my.models.Address;
import org.my.repositories.AddressRepository;
import org.my.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AddressServiceImp implements AddressService {

    @Autowired
    AddressRepository addressRepository;

    public Address create(Address address) {
        return addressRepository.save(address);
    }

    public Address update(Address address) {
        return addressRepository.save(address);
    }

    public void delete(Address address) {
        addressRepository.delete(address);
    }

    public void deleteById(long id) {
        addressRepository.delete(id);
    }

    public Address find(long id) {
        return addressRepository.findOne(id);
    }

    public List<Address> getAll() {
        return addressRepository.findAll();
    }
}
